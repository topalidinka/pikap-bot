Уважаемые разработчики, код написан не в лучших традициях, т.к. это было мое первое знакомство с Laravel. 
Прошу Вас не следовать этому примеру, а улучшить стиль и архитектуру. При этом код абсолютно рабочий, т.е. в некоторых моментах он может выступить для Вас подсказкой

# install dependencies
composer update

# generate key
php artisan key:generate

# migrate db
php artisan migrate
