<?php

namespace App\Telegram;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use App\Http\Controllers\Backend\TelegramController;
use App\Setting;

/**
 * Class StartCommand.
 */
class StartCommand extends Command
{

  /**
   * @var string Command Name
   */
  protected $name = 'start';

  /**
   * @var string Command Description
   */
  protected $description = 'Start command';
  
  /**
   * Handle
   */
  public function handle($arguments) {
  
    $this->replyWithChatAction([
      'action' => Actions::TYPING
    ]);

    $update = $this->getUpdate();

    \Log::info('update');
    \Log::info(print_r($update, true));
    $name   = $update->getMessage()->getFrom()->getFirstName();
    $user   = TelegramController::getUserData($update->getMessage()->getFrom());

    \Log::info('name');
    \Log::info(print_r($name, true));
    $text = sprintf('%s, %s.' . PHP_EOL, 'Приветствую', $name);

    $today    = new \DateTime();
    if (null == $user->subscribe_date) {
      $now      = new \DateTime();
      $last_pay = $now->modify('-31 day');
    } else {
      $last_pay = new \DateTime($user->subscribe_date);
    }
    $interval = $today->diff($last_pay);

    $reply_markup = new Keyboard();
    $reply_markup->inline();

    if ($interval->days > 30) {

      $subscription_cost = Setting::getSettings('subscription_cost');
      $subscription_cost = ($subscription_cost) ? $subscription_cost->value : 0;
      $merchant_id       = Setting::getSettings('merchant_id');
      $merchant_id       = ($merchant_id) ? $merchant_id->value : 0;
      $secret_word       = 'ignetdapassion';
      $order_id          = $update->getMessage()->getFrom()->getId();
      $sign              = md5($merchant_id.':'.$subscription_cost.':'.$secret_word.':'.$order_id);

      $text .= sprintf('%s' . PHP_EOL, 'Вам необходимо оплатить подписку');
      $text .= sprintf('%s' . PHP_EOL, 'Стоимость: ' . $subscription_cost . ' руб/мес');
      $url   = 'http://www.free-kassa.ru/merchant/cash.php?' . 'm=' . $merchant_id . '&oa=' . $subscription_cost . '&o=' . $order_id . '&s=' . $sign . '&us_type=subscribe';  

      $subscribe = Keyboard::inlineButton([
        'text' => 'Оплатить подписку',
        'url'  => $url,
      ]);

      $reply_markup->row(
        $subscribe
      );
    }

    $vebinars = Keyboard::inlineButton([
      'text'          => 'Вебинары',
      'callback_data' => 'vebinars',
    ]);
    
    $reply_markup->row(
      $vebinars
    );

    $response = $this->telegram->sendMessage([
      'chat_id'      => $update->getMessage()->getFrom()->getId(),
      'text'         => $text,
      'reply_markup' => $reply_markup,
    ]);
  }
}
