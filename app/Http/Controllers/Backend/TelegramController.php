<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Api;
use App\TelegramUser;
use App\Chat;
use App\Product;
use App\Order;
use App\Setting;
use Telegram\Bot\Keyboard\Keyboard;

class TelegramController extends Controller
{

 /** @var Api */
 protected $telegram;

  /**
   * BotController constructor.
   *
   * @param Api $telegram
   */
  public function __construct(Api $telegram) {
    
    $this->telegram = $telegram;
  }

  /**
   * Get user data
   */
  public static function getUserData($user_data) {

    $user = TelegramUser::find($user_data->getId());
    if ( ! $user) {
      $user = TelegramUser::create(json_decode($user_data, true));
    }

    return $user;
  }

  /**
   * Handles incoming webhook updates from Telegram.
   */
  public function webhookHandler() {

    $update  = $this->telegram->commandsHandler(true);
    $message = $update->getMessage();

    if (null == $message) {
      $message = $update->getCallbackQuery()->getMessage();
    }

    $from      = $message->getFrom();
    $chat_data = $message->getChat();

    if ('supergroup' == $chat_data->getType()) {

      $chat = Chat::find($chat_data->getId());
      if ( ! $chat) {
        $chat = Chat::create(json_decode($chat_data, true));
      }

      if (null !== $message->getNewChatMember()) {

        $user_data = $message->getNewChatMember();
        $teluser   = self::getUserData($user_data);
        if ($teluser->subscribe_date != null) {

          $sub_date = new \DateTime($teluser->subscribe_date);
          $sub_end  = $sub_date->modify('+30 day');
          $sub_end  = $sub_end->format('Y-m-d');
          $now      = new \DateTime();
          $now      = $now->format('Y-m-d');
          $result   = ($now > $sub_end);

          if ($result) {

            $this->telegram->kickChatMember([
              'chat_id' => $chat_data->getId(),
              'user_id' => $user_data->getId()
            ]);

            $teluser->in_chat = 0;
            $teluser->save();
          } else {

            $teluser->in_chat = 1;
            $teluser->save();
          }
        } else {

          $this->telegram->kickChatMember([
            'chat_id' => $chat_data->getId(),
            'user_id' => $user_data->getId()
          ]);

          $teluser->in_chat = 0;
          $teluser->save();
        }
      }
    }

    if ( ! $from->getIsBot()) {
      $this::getUserData($from);
    }

    if ($update->getCallbackQuery()) {

      $query    = $update->getCallbackQuery();
      $is_exist = method_exists($this, $query->getData());

      if ($is_exist) {
        $this->{$query->getData()}($query);
      }

      $is_view = strripos($query->getData(), 'vbnr_view');
      if ($is_view > -1) {

        $vebinar_id = str_replace('vbnr_view', '', $query->getData());
        $this->vebinar_view($query, $vebinar_id);
      }

      $is_view = strripos($query->getData(), 'vbnr_buy');
      if ($is_view > -1) {

        $vebinar_id = str_replace('vbnr_buy', '', $query->getData());
        $this->vebinar_buy($query, $vebinar_id);
      }
    }

   return 'Ok';
  }

  /**
   * Vebinars button handler
   */
  public function vebinars($query) {

    $text = 'Выберите действие';

    $vebinar_buy = Keyboard::inlineButton([
      'text'          => 'Купить вебинары',
      'callback_data' => 'buy_vebinars',
    ]);

    $my_vebinars = Keyboard::inlineButton([
      'text'          => 'Мои покупки',
      'callback_data' => 'my_vebinars',
    ]);

    $reply_markup = new Keyboard();
    $reply_markup->inline();
    $reply_markup->row(
      $vebinar_buy,
      $my_vebinars
    );

    $response = $this->telegram->sendMessage([
      'chat_id'      => $query->getFrom()->getId(),
      'text'         => $text,
      'reply_markup' => $reply_markup
    ]);

    $response = $this->telegram->answerCallbackQuery([
      'callback_query_id' => $query->getId()
    ]);
  }

  /**
   * Buy vebinars handler
   */
  public function buy_vebinars($query) {

    $user          = TelegramUser::find($query->getFrom()->getId());
    $paid_vebinars = unserialize($user->products);
    $paid_vebinars = ( ! is_array($paid_vebinars)) ? [] : $paid_vebinars;
    $vebinars      = Product::whereNotIn('id', $paid_vebinars)->where('status', '=', 1)->get()->toArray();

    if ( ! empty($vebinars)) {  
        
      $reply_markup = new Keyboard();
      $reply_markup->inline();
      $text = 'Выберите вебинар из списка';
      foreach ($vebinars as $vebinar) {

        $vebinar_btn = Keyboard::inlineButton([
          'text'          => $vebinar['title'],
          'callback_data' => 'vbnr_view' . $vebinar['id'],
        ]);
    
        $reply_markup->row(
          $vebinar_btn
        );
      }

      $response = $this->telegram->sendMessage([
        'chat_id'      => $query->getFrom()->getId(),
        'text'         => $text,
        'reply_markup' => $reply_markup
      ]);
    } else {

      $text = 'Нет доступных вебинаров';
      $response = $this->telegram->sendMessage([
        'chat_id'      => $query->getFrom()->getId(),
        'text'         => $text
      ]);
    }

    $response = $this->telegram->answerCallbackQuery([
      'callback_query_id' => $query->getId()
    ]);
  }

  /**
   * My vebinar handler
   */
  public function my_vebinars($query) {

    $text = 'Веберите вебинар из списка';

    $reply_markup = new Keyboard();
    $reply_markup->inline();

    $user_id       = $query->getFrom()->getId();
    $user          = TelegramUser::find($user_id);
    $orders        = Order::where('status', '=', 1)->where('telegram_user_id', '=', $user_id)->where('description', '!=', '')->get()->toArray();

    if (empty($orders)) {

      $text = 'У вас еще нет купленных вебинаров';

      $vebinar_buy = Keyboard::inlineButton([
        'text'          => 'Купить вебинары',
        'callback_data' => 'buy_vebinars',
      ]);
  
      $reply_markup->row(
        $vebinar_buy
      );
    } else {

      $vebinars = [];
      foreach ($orders as $order) {

        $products = unserialize($order['products']);
        $products = ( ! is_array($products)) ? [] : $products;
        $vebinars = Product::whereIn('id', $products)->get()->toArray();
        $url      = ( ! empty($order['description'])) ? $order['description'] : '';
        foreach ($vebinars as $vebinar) {

          $vebinar_btn = Keyboard::inlineButton([
            'text' => 'Открыть вебинар ' . $vebinar['title'],
            'url'  => $url,
          ]);
      
          $reply_markup->row(
            $vebinar_btn
          );
        }
      }
    }

    $response = $this->telegram->sendMessage([
      'chat_id'      => $query->getFrom()->getId(),
      'text'         => $text,
      'reply_markup' => $reply_markup
    ]);

    $response = $this->telegram->answerCallbackQuery([
      'callback_query_id' => $query->getId()
    ]);
  }

  /**
   * View vebinar handler
   */
  public function vebinar_view($query, $id) {

    $vebinar = Product::find($id);
    $params  = [
      'chat_id' => $query->getFrom()->getId(),
    ];
    
    if ($vebinar) {

      $text  = 'Вебинар "' . $vebinar['title'] . '"'. PHP_EOL;
      $text .= 'Цена: ' . $vebinar['price'] . ' руб' . PHP_EOL;
      $text .= 'Описание: ' . $vebinar['description'] . PHP_EOL;
      
      $reply_markup = new Keyboard();
      $reply_markup->inline();
  
      $vebinar_btn = Keyboard::inlineButton([
        'text'          => 'Оформить заказ',
        'callback_data' => 'vbnr_buy' . $vebinar['id'],
      ]);
  
      $reply_markup->row(
        $vebinar_btn
      );

      $params['reply_markup'] = $reply_markup; 
    } else {

      $text = 'Вебинар не найден';
    }
   
    $params['text'] = $text; 

    $response = $this->telegram->sendMessage($params);
    $response = $this->telegram->answerCallbackQuery([
      'callback_query_id' => $query->getId()
    ]);
  }

  /**
   * Buy vebinar handler
   */
  public function vebinar_buy($query, $id) {

    $vebinar = Product::find($id);
    if ( ! $vebinar) {

      $params = [
        'chat_id' => $query->getFrom()->getId(),
        'text'    => 'Вебинар не найден'
      ];
      
      $response = $this->telegram->sendMessage($params);
      $response = $this->telegram->answerCallbackQuery([
        'callback_query_id' => $query->getId()
      ]);
    } else {
        
      $order = Order::create([
        'telegram_user_id' => $query->getFrom()->getId(),
        'amount'           => $vebinar->price,
        'products'         => [$vebinar->id]
      ]);

      $merchant_id = Setting::getSettings('merchant_id');
      $merchant_id = ($merchant_id) ? $merchant_id->value : 0;
      $secret_word  = 'ignetdapassion';
      $order_id     = $order->id;
      $order_amount = $order->amount;
      $sign         = md5($merchant_id.':'.$order_amount.':'.$secret_word.':'.$order_id);
      
      $text  = 'Заказ #' . $order_id . PHP_EOL;
      $text .= 'Сумма: ' . $order_amount . ' руб' . PHP_EOL; 
      $url   = 'http://www.free-kassa.ru/merchant/cash.php?' . 'm=' . $merchant_id . '&oa=' . $order_amount . '&o=' . $order_id . '&s=' . $sign . '&us_type=vebinar';  

      $reply_markup = new Keyboard();
      $reply_markup->inline();

      $vebinar_btn = Keyboard::inlineButton([
        'text' => 'Оплатить',
        'url'  => $url,
      ]);

      $reply_markup->row(
        $vebinar_btn
      );

      $params = [
        'chat_id'      => $query->getFrom()->getId(),
        'text'         => $text,
        'reply_markup' => $reply_markup,
      ];
      
      $response = $this->telegram->sendMessage($params);
      $response = $this->telegram->answerCallbackQuery([
        'callback_query_id' => $query->getId()
      ]);
    }
  }
}
