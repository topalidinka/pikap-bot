<?php

use Illuminate\Database\Seeder;

class TelUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('telegram_users')->insert([
        'id'       => 672388868,
        'is_bot'   => 0,
        'first_name' => 'Sofia',
      ]);
    }
}
